package io.wojto.songstralala.utils

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.core.internal.deps.guava.base.Preconditions.checkNotNull
import androidx.test.espresso.matcher.BoundedMatcher
import org.hamcrest.Description
import org.hamcrest.Matcher

object TestHelpers {

    fun atPosition(
        position: Int,
        itemMatcher: Matcher<View>
    ): Matcher<View> {
        checkNotNull(itemMatcher)
        return object : BoundedMatcher<View, RecyclerView>(RecyclerView::class.java) {
            override fun describeTo(description: Description) {
                description.appendText("has item at position $position: ")
                itemMatcher.describeTo(description)
            }

            override fun matchesSafely(view: RecyclerView): Boolean {
                val viewHolder = view.findViewHolderForAdapterPosition(position)
                    ?: // has no item on such position
                    return false
                return itemMatcher.matches(viewHolder.itemView)
            }
        }
    }

    fun getJsonFile(fileName: String): String {
        return requireNotNull(javaClass.classLoader)
            .getResourceAsStream(fileName).bufferedReader().use { it.readText() }
    }
}
