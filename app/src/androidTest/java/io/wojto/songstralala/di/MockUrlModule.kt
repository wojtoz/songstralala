package io.wojto.songstralala.di

import dagger.Module
import dagger.Provides
import okhttp3.mockwebserver.MockWebServer
import javax.inject.Named
import javax.inject.Singleton

const val MOCKED_BASE_URL = "mockedBaseUrl"

@Module
class MockUrlModule {

    @Provides
    @Singleton
    fun provideMockServer(): MockWebServer {
        var mockWebServer: MockWebServer? = null

        Thread(Runnable {
            mockWebServer = MockWebServer()
            mockWebServer?.start()
        }).apply {
            start()
            join()
        }

        return requireNotNull(mockWebServer)
    }

    @Provides
    @Singleton
    @Named(MOCKED_BASE_URL)
    fun provideBaseUrl(mockWebServer: MockWebServer): String {
        var url: String? = null

        Thread(Runnable {
            url = mockWebServer.url("/").toString()
        }).apply {
            start()
            join()
        }

        return requireNotNull(url)
    }
}