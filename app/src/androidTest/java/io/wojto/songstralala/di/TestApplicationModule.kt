package io.wojto.songstralala.di

import android.content.res.Resources
import dagger.Module
import dagger.Provides
import io.wojto.songstralala.data.songs.network.api.SongsApi
import io.wojto.songstralala.data.songs.provider.LocalSongsFileProvider
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import javax.inject.Named
import javax.inject.Singleton

private const val MOCKED_RESOURCES = "MOCKED_RESOURCES"

@Module
class TestApplicationModule {

    @Provides
    @Singleton
    fun provideRetrofit(@Named(MOCKED_BASE_URL) baseUrl: String): Retrofit = Retrofit.Builder()
        .baseUrl(baseUrl)
        .client(OkHttpClient.Builder().addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }).build())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()

    @Provides
    @Singleton
    fun provideSongsApi(retrofit: Retrofit): SongsApi =
        retrofit.create(SongsApi::class.java)

    @Provides
    @Singleton
    @Named(MOCKED_RESOURCES)
    fun provideMockedResources(): Resources {
        val mockedResources: Resources = mock(Resources::class.java)
        `when`(mockedResources.openRawResource(ArgumentMatchers.any(Int::class.java))).thenAnswer { invocation ->
            requireNotNull(javaClass.classLoader)
                .getResourceAsStream("TestLocalSongsJson.json")
        }
        return mockedResources
    }

    @Provides
    @Singleton
    fun provideTestLocalSongsFileProvider(
        @Named(MOCKED_RESOURCES) resources: Resources,
        @Named(PACKAGE_NAME) packageName: String
    ): LocalSongsFileProvider {
        return LocalSongsFileProvider(resources, packageName)
    }
}