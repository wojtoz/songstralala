package io.wojto.songstralala.di

import com.squareup.moshi.JsonAdapter
import dagger.Component
import io.wojto.songstralala.data.songs.repository.LocalSong
import io.wojto.songstralala.data.songs.repository.RemoteSongsWrapper
import io.wojto.songstralala.presentation.TestSongsTralalaApplication
import okhttp3.mockwebserver.MockWebServer
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, DataBindsModule::class, TestApplicationModule::class, MoshiModule::class, MockUrlModule::class])
interface TestComponent : ApplicationComponent {

    fun getMockWebServer(): MockWebServer
    fun getLocalSongJsonAdapter(): JsonAdapter<List<LocalSong>>
    fun getRemoteSongJsonAdapter(): JsonAdapter<RemoteSongsWrapper>

    fun inject(testSongsTralalaApplication: TestSongsTralalaApplication)
}