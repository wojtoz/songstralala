package io.wojto.songstralala.data.adapters

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.squareup.moshi.JsonAdapter
import io.wojto.songstralala.data.songs.repository.LocalSong
import io.wojto.songstralala.presentation.HostActivity
import io.wojto.songstralala.presentation.TestSongsTralalaApplication
import io.wojto.songstralala.utils.TestHelpers
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class LocalSongsMoshiAdapterTest {

    @get:Rule
    val activityRule = ActivityTestRule(HostActivity::class.java, true, false)

    private lateinit var target: JsonAdapter<List<LocalSong>>

    @Before
    fun setUp() {
        val instrumentation = InstrumentationRegistry.getInstrumentation()
        val app = instrumentation.targetContext.applicationContext as TestSongsTralalaApplication
        target = app.applicationComponent.getLocalSongJsonAdapter()
    }

    @Test
    fun shouldCorrectlyParseLocalSongs() {
        val fromJson = target.fromJson(TestHelpers.getJsonFile("TestLocalSongsJson.json"))
        Assert.assertTrue(VALID_LOCAL_TEST_SONGS == fromJson)
    }

    companion object {
        private val VALID_LOCAL_TEST_SONGS = listOf(
            LocalSong("Caught Up in You", ".38 Special", "1982"),
            LocalSong("CAN'T STOP ROCK'N'ROLL", "AC/DC", ""),
            LocalSong("You Better Run", "Pat Benatar", "1980")
        )
    }
}