package io.wojto.songstralala.data.adapters

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.squareup.moshi.JsonAdapter
import io.wojto.songstralala.data.songs.repository.RemoteSong
import io.wojto.songstralala.data.songs.repository.RemoteSongsWrapper
import io.wojto.songstralala.presentation.HostActivity
import io.wojto.songstralala.presentation.TestSongsTralalaApplication
import io.wojto.songstralala.utils.TestHelpers
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.sql.Date

class RemoteSongsMoshiAdapterTest {

    @get:Rule
    val activityRule = ActivityTestRule(HostActivity::class.java, true, false)

    private lateinit var target: JsonAdapter<RemoteSongsWrapper>

    @Before
    fun setUp() {
        val instrumentation = InstrumentationRegistry.getInstrumentation()
        val app = instrumentation.targetContext.applicationContext as TestSongsTralalaApplication
        target = app.applicationComponent.getRemoteSongJsonAdapter()
    }

    @Test
    fun shouldCorrectlyParseLocalSongs() {
        val fromJson: RemoteSongsWrapper = target.fromJson(TestHelpers.getJsonFile("TestRemoteSongsJson.json"))!!
        Assert.assertTrue(VALID_REMOTE_TEST_SONGS_WRAPPER == fromJson)
    }

    companion object {
        private val VALID_REMOTE_TEST_SONGS_WRAPPER = RemoteSongsWrapper(
            2, listOf(
                RemoteSong("Better Together", "Jack Johnson", Date.valueOf("2005-03-1")),
                RemoteSong("Upside Down", "Jack Johnson", Date.valueOf("2006-02-06"))
            )
        )
    }
}