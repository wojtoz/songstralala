package io.wojto.songstralala.presentation

import io.wojto.songstralala.SongsTralalaApplication
import io.wojto.songstralala.di.ApplicationModule
import io.wojto.songstralala.di.DaggerTestComponent
import io.wojto.songstralala.di.TestApplicationModule
import io.wojto.songstralala.di.TestComponent

class TestSongsTralalaApplication : SongsTralalaApplication() {

    override val applicationComponent: TestComponent by lazy {
        DaggerTestComponent.builder()
            .applicationModule(ApplicationModule(this))
            .testApplicationModule(TestApplicationModule())
            .build()
    }
}