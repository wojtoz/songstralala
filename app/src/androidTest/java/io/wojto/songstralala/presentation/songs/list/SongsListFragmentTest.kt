package io.wojto.songstralala.presentation.songs.list

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.hasDescendant
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import io.wojto.songstralala.R
import io.wojto.songstralala.domain.songs.entities.Song
import io.wojto.songstralala.presentation.HostActivity
import io.wojto.songstralala.presentation.TestSongsTralalaApplication
import io.wojto.songstralala.utils.RecyclerViewItemCountAssertion
import io.wojto.songstralala.utils.TestHelpers
import io.wojto.songstralala.utils.TestHelpers.atPosition
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SongsListFragmentTest {

    @get:Rule
    val activityRule = ActivityTestRule(HostActivity::class.java, true, false)

    private lateinit var mockedWebServer: MockWebServer

    @Before
    fun setUp() {
        val instrumentation = InstrumentationRegistry.getInstrumentation()
        val app = instrumentation.targetContext.applicationContext as TestSongsTralalaApplication
        mockedWebServer = app.applicationComponent.getMockWebServer()

        mockedWebServer.setDispatcher(object : Dispatcher() {
            override fun dispatch(request: RecordedRequest?): MockResponse {
                return MockResponse().setBody(TestHelpers.getJsonFile("TestRemoteSongsJson.json"))
            }
        })
    }

    @Test
    fun shouldShowBothRemoteAndLocalSongsOnAppStartup() {
        //when
        activityRule.launchActivity(null)

        //then
        checkIfRecyclerViewContainsSongs(TEST_LOCAL_SONGS + TEST_REMOTE_SONGS)
    }

    @Test
    fun shouldShowOnlyRemoteSongsOnLocalUnchecked() {
        //when
        activityRule.launchActivity(null)
        onView(withId(R.id.local_filter_chip)).perform(ViewActions.click())

        //then
        checkIfRecyclerViewContainsSongs(TEST_REMOTE_SONGS)
    }

    @Test
    fun shouldShowOnlyLocalSongsOnRemoteUnchecked() {
        //when
        activityRule.launchActivity(null)
        onView(withId(R.id.remote_filter_chip)).perform(ViewActions.click())

        //then
        checkIfRecyclerViewContainsSongs(TEST_LOCAL_SONGS)
    }

    private fun checkIfRecyclerViewContainsSongs(songs: List<Song>) {
        val recyclerViewInteraction = onView(withId(R.id.songs_recycler_view))

        recyclerViewInteraction.check(RecyclerViewItemCountAssertion(songs.size))
        songs.forEachIndexed { index, song ->
            recyclerViewInteraction
                .check(matches(atPosition(index, hasDescendant(withText(song.name)))))
            recyclerViewInteraction
                .check(matches(atPosition(index, hasDescendant(withText(song.artist)))))
            song.releaseYear?.let {
                recyclerViewInteraction
                    .check(matches(atPosition(index, hasDescendant(withText(it)))))
            }
        }
    }

    companion object {

        private val TEST_LOCAL_SONGS = listOf(
            Song("Caught Up in You", ".38 Special", "1982"),
            Song("CAN'T STOP ROCK'N'ROLL", "AC/DC", null),
            Song("You Better Run", "Pat Benatar", "1980")
        )

        private val TEST_REMOTE_SONGS = listOf(
            Song("Better Together", "Jack Johnson", "2005"),
            Song("Upside Down", "Jack Johnson", "2006")
        )
    }
}



