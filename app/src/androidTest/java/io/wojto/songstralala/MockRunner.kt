package io.wojto.songstralala

import android.app.Application
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner
import io.wojto.songstralala.presentation.TestSongsTralalaApplication

class MockRunner : AndroidJUnitRunner() {

    @Throws(InstantiationException::class, IllegalAccessException::class, ClassNotFoundException::class)
    override fun newApplication(cl: ClassLoader, className: String, context: Context): Application {
        return super.newApplication(cl, TestSongsTralalaApplication::class.java.name, context)
    }
}