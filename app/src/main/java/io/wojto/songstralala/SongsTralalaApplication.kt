package io.wojto.songstralala

import android.app.Application
import io.wojto.songstralala.di.ApplicationComponent
import io.wojto.songstralala.di.ApplicationModule
import io.wojto.songstralala.di.DaggerApplicationComponent
import io.wojto.songstralala.di.MoshiModule
import io.wojto.songstralala.di.RetrofitModule

open class SongsTralalaApplication : Application() {

    open val applicationComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .retrofitModule(RetrofitModule())
            .moshiModule(MoshiModule())
            .build()
    }
}