package io.wojto.songstralala.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import io.wojto.songstralala.R
import io.wojto.songstralala.SongsTralalaApplication
import io.wojto.songstralala.di.ActivityComponent
import io.wojto.songstralala.di.ActivityModule
import io.wojto.songstralala.di.DaggerActivityComponent
import io.wojto.songstralala.presentation.songs.list.SongsListFragment

class HostActivity : AppCompatActivity() {

    val activityComponent: ActivityComponent by lazy {
        DaggerActivityComponent.builder()
            .applicationComponent((application as SongsTralalaApplication).applicationComponent)
            .activityModule(ActivityModule())
            .build()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activityComponent.inject(this)
        setContentView(R.layout.activity_host)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .add(R.id.fragment_container, SongsListFragment())
                .commit()
        }
    }
}
