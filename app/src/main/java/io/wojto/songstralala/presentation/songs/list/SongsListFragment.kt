package io.wojto.songstralala.presentation.songs.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import io.wojto.songstralala.databinding.FragmentSongsListBinding
import io.wojto.songstralala.presentation.HostActivity
import io.wojto.songstralala.presentation.songs.list.di.DaggerSongsListComponent
import javax.inject.Inject

class SongsListFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: SongsListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        DaggerSongsListComponent.builder()
            .activityComponent((activity as HostActivity).activityComponent)
            .build()
            .inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SongsListViewModel::class.java)

        return FragmentSongsListBinding.inflate(inflater).apply {
            lifecycleOwner = this@SongsListFragment
            viewModel = this@SongsListFragment.viewModel

            val dividerItemDecoration = DividerItemDecoration(
                songsRecyclerView.context,
                DividerItemDecoration.VERTICAL
            )
            songsRecyclerView.addItemDecoration(dividerItemDecoration)
        }.root
    }
}
