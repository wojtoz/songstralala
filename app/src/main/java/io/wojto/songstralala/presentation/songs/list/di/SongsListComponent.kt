package io.wojto.songstralala.presentation.songs.list.di

import dagger.Component
import io.wojto.songstralala.di.ActivityComponent
import io.wojto.songstralala.di.FragmentScope
import io.wojto.songstralala.presentation.songs.list.SongsListFragment

@FragmentScope
@Component(dependencies = [ActivityComponent::class])
interface SongsListComponent {

    fun inject(songsListFragment: SongsListFragment)
}
