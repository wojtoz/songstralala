package io.wojto.songstralala.presentation.songs.list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.DiffUtil
import io.reactivex.observers.DisposableObserver
import io.wojto.songstralala.BR
import io.wojto.songstralala.R
import io.wojto.songstralala.di.ActivityScope
import io.wojto.songstralala.domain.songs.entities.Song
import io.wojto.songstralala.domain.songs.interactors.GetSongsUseCase
import io.wojto.songstralala.domain.songs.interactors.GetSongsUseCaseParam
import me.tatarka.bindingcollectionadapter2.ItemBinding
import me.tatarka.bindingcollectionadapter2.collections.DiffObservableList
import javax.inject.Inject

@ActivityScope
class SongsListViewModel @Inject constructor(
    private val getSongsUseCase: GetSongsUseCase
) : ViewModel() {

    val localSongsSelected = MutableLiveData<Boolean>().apply {
        value = true
    }
    val remoteSongsSelected = MutableLiveData<Boolean>().apply {
        value = true
    }

    val songsListBinding = ItemBinding.of<Song>(BR.song, R.layout.song_item)
    val songsList = DiffObservableList<Song>(object : DiffUtil.ItemCallback<Song?>() {
        override fun areItemsTheSame(oldItem: Song, newItem: Song) = oldItem == newItem
        override fun areContentsTheSame(oldItem: Song, newItem: Song) = oldItem == newItem
    })

    init {
        reloadSongsList()
    }

    fun onRemoteSongsFilterChange(remoteSongsFilterChecked: Boolean) {
        remoteSongsSelected.value = remoteSongsFilterChecked
        reloadSongsList()
    }

    fun onLocalSongsFilterChange(localSongsFilterChecked: Boolean) {
        localSongsSelected.value = localSongsFilterChecked
        reloadSongsList()
    }

    private fun reloadSongsList() {
        getSongsUseCase.unsubscribe()
        getSongsUseCase.execute(
            GetSongsUseCaseParam(
                getLocal = localSongsSelected.value ?: false,
                getRemote = remoteSongsSelected.value ?: false
            ),
            object : DisposableObserver<List<Song>>() {
                override fun onComplete() {
                    //no-op
                }

                override fun onNext(songs: List<Song>) {
                    songsList.update(songs)
                }

                override fun onError(e: Throwable) {
                    //no-op
                }
            }
        )
    }

    override fun onCleared() {
        getSongsUseCase.unsubscribe()
    }
}
