package io.wojto.songstralala.data.songs.repository

import com.squareup.moshi.Json
import com.squareup.moshi.JsonAdapter
import io.reactivex.Flowable
import io.reactivex.Single
import io.wojto.songstralala.data.songs.provider.LocalSongsFileProvider
import io.wojto.songstralala.domain.songs.entities.Song
import io.wojto.songstralala.domain.songs.repository.SongsRepository
import java.io.BufferedReader
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LocalSongsRepository @Inject constructor(
    private val localSongsFileProvider: LocalSongsFileProvider,
    private val localSongsJsonAdapter: JsonAdapter<List<LocalSong>>
) : SongsRepository {

    override fun getSongs(): Single<List<Song>> {
        return Flowable.using(
            { BufferedReader(localSongsFileProvider.getLocalSongsFileStream().reader()) },
            { reader ->
                Flowable.fromCallable<String> {
                    reader.readText()
                }
            },
            { reader -> reader.close() }
        ).map { localSongsRawJson ->
            localSongsJsonAdapter.fromJson(localSongsRawJson)
                ?.map {
                    it.toSong()
                }.orEmpty()
        }.firstOrError()
    }
}

data class LocalSong(
    @Json(name = "Song Clean") val localName: String,
    @Json(name = "ARTIST CLEAN") val localArtist: String,
    @Json(name = "Release Year") val localYear: String
) {
    fun toSong() = Song(localName, localArtist, if (localYear.isBlank()) null else localYear)
}