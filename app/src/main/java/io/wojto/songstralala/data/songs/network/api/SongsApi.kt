package io.wojto.songstralala.data.songs.network.api

import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Query

interface SongsApi {

    @GET("search")
    fun getSongs(@Query("term") term: String, @Query("limit") limit: Int): Single<ResponseBody>
}