package io.wojto.songstralala.data.songs.repository

import com.squareup.moshi.Json
import com.squareup.moshi.JsonAdapter
import io.reactivex.Single
import io.wojto.songstralala.data.songs.network.api.SongsApi
import io.wojto.songstralala.domain.songs.entities.Song
import io.wojto.songstralala.domain.songs.repository.SongsRepository
import java.util.Calendar
import java.util.Date
import javax.inject.Inject
import javax.inject.Singleton

//To change, depends on problem's domain
private const val DUMMY_SEARCH_QUERY = "Michael Jackson"
private const val DUMMY_SEARCH_LIMIT = 2

@Singleton
class RemoteSongsRepository @Inject constructor(
    private val songsApi: SongsApi,
    private val remoteSongsWrapperJsonAdapter: JsonAdapter<RemoteSongsWrapper>
) : SongsRepository {

    override fun getSongs(): Single<List<Song>> {
        return songsApi.getSongs(
            DUMMY_SEARCH_QUERY,
            DUMMY_SEARCH_LIMIT
        ).map { responseBody ->
            remoteSongsWrapperJsonAdapter.fromJson(responseBody.string())
                ?.results
                ?.map { remoteSong ->
                    remoteSong.toSong()
                } ?: emptyList()
        }
    }
}

data class RemoteSongsWrapper(
    @Json(name = "resultCount") val resultCount: Int,
    @Json(name = "results") val results: List<RemoteSong>
)

data class RemoteSong(
    @Json(name = "trackName") val remoteName: String,
    @Json(name = "artistName") val remoteArtist: String,
    @Json(name = "releaseDate") val releaseDate: Date
) {
    fun toSong(): Song {
        val calendar = Calendar.getInstance()
        calendar.time = releaseDate
        return Song(remoteName, remoteArtist, calendar.get(Calendar.YEAR).toString())
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RemoteSong

        if (remoteName != other.remoteName) return false
        if (remoteArtist != other.remoteArtist) return false

        return true
    }

    override fun hashCode(): Int {
        var result = remoteName.hashCode()
        result = 31 * result + remoteArtist.hashCode()
        return result
    }
}