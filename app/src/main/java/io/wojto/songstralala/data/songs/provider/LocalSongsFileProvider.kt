package io.wojto.songstralala.data.songs.provider

import android.content.res.Resources
import io.wojto.songstralala.di.ActivityScope
import io.wojto.songstralala.di.PACKAGE_NAME
import java.io.InputStream
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

private const val LOCAL_FILE_SONGS_NAME = "local_songs"

@Singleton
class LocalSongsFileProvider @Inject constructor(
    private val resources: Resources,
    @Named(PACKAGE_NAME) private val packageName: String
) {
    fun getLocalSongsFileStream(): InputStream = resources.openRawResource(
        resources.getIdentifier(
            LOCAL_FILE_SONGS_NAME,
            "raw",
            packageName
        )
    )
}