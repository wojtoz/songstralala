package io.wojto.songstralala.domain.songs.interactors

import io.reactivex.Observable
import io.reactivex.Observable.merge
import io.reactivex.Single
import io.wojto.songstralala.di.INTERNAL_IO
import io.wojto.songstralala.di.LOCAL_REPOSITORY
import io.wojto.songstralala.di.REMOTE_REPOSITORY
import io.wojto.songstralala.domain.schedulers.SchedulerProvider
import io.wojto.songstralala.domain.songs.entities.Song
import io.wojto.songstralala.domain.songs.repository.SongsRepository
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class GetSongsUseCase @Inject constructor(
    @Named(INTERNAL_IO) schedulerProvider: SchedulerProvider,
    @Named(REMOTE_REPOSITORY) private val remoteSongsRepository: SongsRepository,
    @Named(LOCAL_REPOSITORY) private val localSongsRepository: SongsRepository
) : UseCase<GetSongsUseCaseParam, List<Song>>(schedulerProvider) {

    override fun buildUseCaseObservable(
        param: GetSongsUseCaseParam
    ): Observable<List<Song>> {
        if (param.getLocal.not() and param.getRemote.not()) {
            return Single.just(emptyList<Song>()).toObservable()
        }

        return merge(
            if (param.getLocal) localSongsRepository.getSongs().toObservable() else Observable.empty(),
            if (param.getRemote) remoteSongsRepository.getSongs().toObservable() else Observable.empty()
        ).toList().map {
            it.flatten()
        }.toObservable()
    }
}

data class GetSongsUseCaseParam(val getLocal: Boolean, val getRemote: Boolean)