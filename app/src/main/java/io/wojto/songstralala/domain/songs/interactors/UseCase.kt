package io.wojto.songstralala.domain.songs.interactors

import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.disposables.Disposables
import io.wojto.songstralala.domain.schedulers.SchedulerProvider

/**
 * Abstract class for a Use Case (Interactor in terms of Clean Architecture).
 * This interface represents a execution unit for different use cases (this means any use case
 * in the application should implement this contract).
 *
 *
 * By convention each UseCase implementation will return the result using a [Observer]
 * that will execute its job in a background thread and will post the result in the UI thread.
 *
 *
 * Class is parameterized with two types, where:
 * P denotes type for entry arguments,
 * R denotes result type
 */
abstract class UseCase<P, R> {

    private val schedulerProvider: SchedulerProvider?
    private var disposable: Disposable? = Disposables.empty()
    private var observable: Observable<R>? = null

    /**
     * Checks if observable is currently executing a job.
     *
     * @return true if observable is active, false otherwise
     */
    val isExecuting: Boolean
        get() = observable != null

    protected constructor() {
        this.schedulerProvider = null
    }

    protected constructor(schedulerProvider: SchedulerProvider) {
        this.schedulerProvider = schedulerProvider
    }

    /**
     * Executes the current use case.
     *
     * @param observer The guy who will be listen to the observable build with [.buildUseCaseObservable].
     */
    fun <K> execute(
        param: P,
        observer: K
    ) where K : Observer<R>, K : Disposable {
        if (isExecuting) {
            unsubscribe()
            disposable = null
            observable = null
        }

        this.observable = buildUseCaseObservable(param)

        process(observable, observer)
    }

    /**
     * Unsubscribes from current [Disposable].
     */
    fun unsubscribe() {
        if (disposable != null && !disposable!!.isDisposed) {
            disposable!!.dispose()
        }
    }

    /**
     * Builds an [Observable] which will be used when executing the current [UseCase].
     */
    protected abstract fun buildUseCaseObservable(param: P): Observable<R>

    private fun <K> process(
        observable: Observable<R>?,
        observer: K
    ) where K : Observer<R>, K : Disposable {
        val sourceObservable = applySchedulers(observable)

        this.disposable = observer

        sourceObservable!!
            .doOnTerminate { this.observable = null }
            .subscribe(observer)
    }

    protected fun applySchedulers(sourceObservable: Observable<R>?): Observable<R>? {
        var resultObservable = sourceObservable
        if (schedulerProvider != null) {
            resultObservable = resultObservable!!
                .subscribeOn(schedulerProvider.subscribeOnScheduler)
                .observeOn(schedulerProvider.observeOnScheduler)
        }
        return resultObservable
    }
}
