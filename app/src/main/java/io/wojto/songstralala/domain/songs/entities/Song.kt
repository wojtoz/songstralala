package io.wojto.songstralala.domain.songs.entities

data class Song(
    val name: String,
    val artist: String,
    val releaseYear: String?
)