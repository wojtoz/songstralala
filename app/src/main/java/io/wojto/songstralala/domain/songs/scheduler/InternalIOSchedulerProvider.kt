package io.wojto.songstralala.domain.songs.scheduler

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.wojto.songstralala.domain.schedulers.SchedulerProvider

class InternalIOSchedulerProvider : SchedulerProvider {

    override val observeOnScheduler: Scheduler
        get() = AndroidSchedulers.mainThread()

    override val subscribeOnScheduler: Scheduler
        get() = Schedulers.io()
}
