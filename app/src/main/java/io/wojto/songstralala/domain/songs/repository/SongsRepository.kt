package io.wojto.songstralala.domain.songs.repository

import io.reactivex.Single
import io.wojto.songstralala.domain.songs.entities.Song

interface SongsRepository {

    fun getSongs(): Single<List<Song>>
}