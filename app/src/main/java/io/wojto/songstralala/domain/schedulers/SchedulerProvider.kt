package io.wojto.songstralala.domain.schedulers

import io.reactivex.Scheduler

interface SchedulerProvider {
    val observeOnScheduler: Scheduler
    val subscribeOnScheduler: Scheduler
}
