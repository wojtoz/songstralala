package io.wojto.songstralala.di

import androidx.lifecycle.ViewModelProvider
import dagger.Component
import io.wojto.songstralala.presentation.HostActivity

@ActivityScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [ActivityModule::class]
)
interface ActivityComponent {
    fun songsTralalaViewModelFactory(): ViewModelProvider.Factory

    fun inject(hostActivity: HostActivity)
}