package io.wojto.songstralala.di

import dagger.Module

const val INTERNAL_IO = "INTERNAL_IO"
const val REMOTE_REPOSITORY = "REMOTE_REPOSITORY"
const val LOCAL_REPOSITORY = "LOCAL_REPOSITORY"
const val PACKAGE_NAME = "PACKAGE_NAME"

@Module(includes = [ViewModelBindsModule::class])
class ActivityModule
