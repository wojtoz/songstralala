package io.wojto.songstralala.di

import dagger.Component
import io.wojto.songstralala.SongsTralalaApplication
import io.wojto.songstralala.domain.songs.interactors.GetSongsUseCase
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, MoshiModule::class, RetrofitModule::class, DataBindsModule::class])
interface ApplicationComponent {
    fun getSongsUseCase(): GetSongsUseCase

    fun inject(songsTralalaApplication: SongsTralalaApplication)
}