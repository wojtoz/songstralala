package io.wojto.songstralala.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import io.wojto.songstralala.presentation.songs.list.SongsListViewModel
import io.wojto.songstralala.presentation.viewmodel.SongsTralalaViewModelFactory

@Suppress("unused")
@Module
abstract class ViewModelBindsModule {

    @Binds
    @IntoMap
    @ViewModelKey(SongsListViewModel::class)
    abstract fun bindSongsListViewModel(
        songsListViewModel: SongsListViewModel
    ): ViewModel

    @Binds
    abstract fun bindViewModelFactory(
        factory: SongsTralalaViewModelFactory
    ): ViewModelProvider.Factory
}