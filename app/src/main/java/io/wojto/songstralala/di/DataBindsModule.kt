package io.wojto.songstralala.di

import dagger.Binds
import dagger.Module
import io.wojto.songstralala.data.songs.repository.LocalSongsRepository
import io.wojto.songstralala.data.songs.repository.RemoteSongsRepository
import io.wojto.songstralala.domain.songs.repository.SongsRepository
import javax.inject.Named

@Module
abstract class DataBindsModule {

    @Binds
    @Named(REMOTE_REPOSITORY)
    abstract fun bindRemoteRepository(
        remoteSongsRepository: RemoteSongsRepository
    ): SongsRepository

    @Binds
    @Named(LOCAL_REPOSITORY)
    abstract fun bindSnackbarProvider(
        localSongsRepository: LocalSongsRepository
    ): SongsRepository
}