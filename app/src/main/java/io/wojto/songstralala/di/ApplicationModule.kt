package io.wojto.songstralala.di

import android.app.Application
import android.content.res.Resources
import dagger.Module
import dagger.Provides
import io.wojto.songstralala.domain.schedulers.SchedulerProvider
import io.wojto.songstralala.domain.songs.scheduler.InternalIOSchedulerProvider
import javax.inject.Named
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: Application) {

    @Provides
    @Singleton
    @Named(PACKAGE_NAME)
    fun providePackageName(): String = application.packageName

    @Provides
    @Singleton
    fun provideResources(): Resources = application.resources

    @Provides
    @Singleton
    @Named(INTERNAL_IO)
    fun provideInternalIOSchedulerProvider(): SchedulerProvider {
        return InternalIOSchedulerProvider()
    }
}