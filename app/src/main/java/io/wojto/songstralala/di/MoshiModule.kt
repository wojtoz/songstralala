package io.wojto.songstralala.di

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import io.wojto.songstralala.data.songs.repository.LocalSong
import io.wojto.songstralala.data.songs.repository.RemoteSongsWrapper
import java.util.Date
import javax.inject.Singleton

@Module
class MoshiModule {
    @Provides
    @Singleton
    fun provideMoshi(): Moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .add(Date::class.java, Rfc3339DateJsonAdapter())
        .build()

    @Provides
    @Singleton
    fun provideLocalSongsJsonAdapter(moshi: Moshi): JsonAdapter<List<LocalSong>> {
        val dataType = Types.newParameterizedType(List::class.java, LocalSong::class.java)
        return moshi.adapter<List<LocalSong>>(dataType)
    }

    @Provides
    @Singleton
    fun provideRemoteSongsJsonAdapter(moshi: Moshi): JsonAdapter<RemoteSongsWrapper> {
        return moshi.adapter<RemoteSongsWrapper>(RemoteSongsWrapper::class.java)
    }
}