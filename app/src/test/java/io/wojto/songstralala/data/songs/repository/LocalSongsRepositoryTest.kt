package io.wojto.songstralala.data.songs.repository

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import com.squareup.moshi.JsonAdapter
import io.wojto.songstralala.data.songs.provider.LocalSongsFileProvider
import io.wojto.songstralala.domain.songs.entities.Song
import io.wojto.songstralala.domain.songs.repository.SongsRepository
import org.junit.Before
import org.junit.Test
import java.io.ByteArrayInputStream

class LocalSongsRepositoryTest {

    private val mockedLocalSongsFileProvider: LocalSongsFileProvider = mock()
    private val mockedLocalSongsJsonAdapter: JsonAdapter<List<LocalSong>> = mock()
    private lateinit var target: SongsRepository

    @Before
    fun setUp() {
        target = LocalSongsRepository(mockedLocalSongsFileProvider, mockedLocalSongsJsonAdapter)
    }

    @Test
    fun `should correctly get list of songs`() {
        //given
        whenever(mockedLocalSongsFileProvider.getLocalSongsFileStream()).thenReturn(ByteArrayInputStream("mock".toByteArray()))
        whenever(mockedLocalSongsJsonAdapter.fromJson(any<String>())).thenReturn(LOCAL_SONGS)

        //when
        val testObserver = target.getSongs().test()

        //then
        testObserver.assertComplete()
            .assertNoErrors()
            .assertValue {
                it == SONGS
            }
    }

    companion object {
        private val LOCAL_SONGS = listOf(
            LocalSong("Caught Up in You", ".38 Special", "1982"),
            LocalSong("CAN'T STOP ROCK'N'ROLL", "AC/DC", ""),
            LocalSong("You Better Run", "Pat Benatar", "1980")
        )

        private val SONGS = listOf(
            Song("Caught Up in You", ".38 Special", "1982"),
            Song("CAN'T STOP ROCK'N'ROLL", "AC/DC", null),
            Song("You Better Run", "Pat Benatar", "1980")
        )
    }
}