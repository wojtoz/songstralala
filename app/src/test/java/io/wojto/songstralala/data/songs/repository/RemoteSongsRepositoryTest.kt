package io.wojto.songstralala.data.songs.repository

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import com.squareup.moshi.JsonAdapter
import io.reactivex.Single
import io.wojto.songstralala.data.songs.network.api.SongsApi
import io.wojto.songstralala.domain.songs.entities.Song
import io.wojto.songstralala.domain.songs.repository.SongsRepository
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Before
import org.junit.Test
import java.sql.Date

class RemoteSongsRepositoryTest {

    private val mockedSongsApi: SongsApi = mock()
    private val mockedRemoteSongsJsonAdapter: JsonAdapter<RemoteSongsWrapper> = mock()

    private lateinit var target: SongsRepository

    @Before
    fun setUp() {
        target = RemoteSongsRepository(mockedSongsApi, mockedRemoteSongsJsonAdapter)
    }

    @Test
    fun `should properly extract wrapped data`() {
        //given
        whenever(mockedSongsApi.getSongs(any(), any())).thenReturn(Single.just(ResponseBody.create(MediaType.parse("application/json"), "")))
        whenever(mockedRemoteSongsJsonAdapter.fromJson(any<String>())).thenReturn(SAMPLE_SERVER_RESPONSE)

        //when
        val testObserver = target.getSongs().test()

        //then
        testObserver.assertComplete()
            .assertNoErrors()
            .assertValue {
                it == EXPECTED_SONGS
            }
    }

    companion object {
        private val SAMPLE_SERVER_RESPONSE = RemoteSongsWrapper(
            3, listOf(
                RemoteSong("testName", "testArtist", Date.valueOf("2018-08-30"))
            )
        )

        private val EXPECTED_SONGS = listOf(
            Song("testName", "testArtist", "2018")
        )
    }
}