package io.wojto.songstralala

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import io.wojto.songstralala.domain.schedulers.SchedulerProvider

class MockSchedulerProvider : SchedulerProvider {

    override val observeOnScheduler: Scheduler = Schedulers.trampoline()

    override val subscribeOnScheduler: Scheduler
        get() = observeOnScheduler
}
