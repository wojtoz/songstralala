package io.wojto.songstralala.domain.songs.interactors

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.wojto.songstralala.MockSchedulerProvider
import io.wojto.songstralala.domain.songs.entities.Song
import io.wojto.songstralala.domain.songs.repository.SongsRepository
import org.junit.Before
import org.junit.Test

class GetSongsUseCaseTest {

    private lateinit var target: GetSongsUseCase

    private val mockedLocalSongsRepository: SongsRepository = mock()
    private val mockedRemoteSongsRepository: SongsRepository = mock()

    private val observer: TestObserver<List<Song>> = TestObserver.create()

    @Before
    fun setUp() {
        target = GetSongsUseCase(
            MockSchedulerProvider(),
            mockedRemoteSongsRepository,
            mockedLocalSongsRepository
        )

        whenever(mockedRemoteSongsRepository.getSongs()).thenReturn(Single.just(REMOTE_SONGS))
        whenever(mockedLocalSongsRepository.getSongs()).thenReturn(Single.just(LOCAL_SONGS))
    }

    @Test
    fun `should return empty list when both remote and local songs are not requested`() {
        //when
        target.execute(
            GetSongsUseCaseParam(
                getLocal = false,
                getRemote = false
            ),
            observer
        )

        //then
        observer.assertComplete()
            .assertNoErrors()
            .assertValueCount(1)
            .assertValue {
                it.isEmpty()
            }
    }

    @Test
    fun `should return only remote songs when only remote songs are requested`() {
        //when
        target.execute(
            GetSongsUseCaseParam(
                getLocal = false,
                getRemote = true
            ),
            observer
        )

        //then
        observer.assertComplete()
            .assertNoErrors()
            .assertValue(REMOTE_SONGS)
    }

    @Test
    fun `should return only local songs when only local songs are requested`() {
        //when
        target.execute(
            GetSongsUseCaseParam(
                getLocal = true,
                getRemote = false
            ),
            observer
        )

        //then
        observer.assertComplete()
            .assertNoErrors()
            .assertValue(LOCAL_SONGS)
    }

    @Test
    fun `should return both local and remote songs when local and remote songs are requested`() {
        //when
        target.execute(
            GetSongsUseCaseParam(
                getLocal = true,
                getRemote = true
            ),
            observer
        )

        //then
        print(
            observer.assertComplete()
                .assertNoErrors()
                .assertValueCount(1)
                .assertValue(LOCAL_SONGS + REMOTE_SONGS)
        )
    }

    companion object {
        private val REMOTE_SONGS = listOf(
            Song("remoteName", "remoteArtist", "remoteYear")
        )

        private val LOCAL_SONGS = listOf(
            Song("localName", "localArtist", "localYear")
        )
    }
}