package io.wojto.songstralala.presentation.songs.list

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.observers.DisposableObserver
import io.wojto.songstralala.domain.songs.entities.Song
import io.wojto.songstralala.domain.songs.interactors.GetSongsUseCase
import io.wojto.songstralala.domain.songs.interactors.GetSongsUseCaseParam
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class SongsListViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var target: SongsListViewModel

    private val mockedGetSognsUseCase: GetSongsUseCase = mock()

    @Before
    fun setUp() {
        target = SongsListViewModel(mockedGetSognsUseCase)
    }

    @Test
    fun `should get songs from both locale and remote on init`() {
        //then
        verify(mockedGetSognsUseCase).execute(
            eq(
                GetSongsUseCaseParam(
                    getLocal = true,
                    getRemote = true
                )
            ),
            any<DisposableObserver<List<Song>>>()
        )
    }

    @Test
    fun `should not show remote songs when remote is unchecked`() {
        //given
        target.onRemoteSongsFilterChange(false)

        //then
        verify(mockedGetSognsUseCase).execute(
            eq(
                GetSongsUseCaseParam(
                    getLocal = true,
                    getRemote = false
                )
            ),
            any<DisposableObserver<List<Song>>>()
        )
    }

    @Test
    fun `should not show local songs when local is unchecked`() {
        //when
        target.onLocalSongsFilterChange(false)

        //then
        verify(mockedGetSognsUseCase).execute(
            eq(
                GetSongsUseCaseParam(
                    getLocal = false,
                    getRemote = true
                )
            ),
            any<DisposableObserver<List<Song>>>()
        )
    }

    @Test
    fun `should show not songs when local and remote are unchecked`() {
        //when
        target.onLocalSongsFilterChange(false)
        target.onRemoteSongsFilterChange(false)

        //then
        verify(mockedGetSognsUseCase).execute(
            eq(
                GetSongsUseCaseParam(
                    getLocal = false,
                    getRemote = false
                )
            ),
            any<DisposableObserver<List<Song>>>()
        )
    }
}